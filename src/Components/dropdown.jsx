import React from "react";
import { Link } from "react-router-dom";

function Dropdown(props){

    return(
        <>
        <div className="w-full flex md:hidden bg-red-950">
            <span className="text-red-300 px-5">Upto 80% discount Just for you!!! Hurry UP...</span>
        </div>
        <div className="bg-red-950">
            <div className="headerbottom container m-auto gap-5 hidden md:flex">
                <div className="dropdown inline-block">
                    <button className="text-xl text-red-300 w-300 px-5 py-5 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-300">Men's Collection</button>
                    <div className="content drop-shadow-2xl hidden absolute min-w-28 text-red-950 bg-red-400">
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Formals</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Casuals</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Party Wears</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Gadgets</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Jackets</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Sports Wears</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Night Dresses</Link>
                    </div>
                </div>
                <div className="dropdown inline-block">
                    <button className="text-xl text-red-300 px-5 py-5 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-300">Ladies Collections</button>
                    <div className="content drop-shadow-2xl hidden absolute min-w-28 text-red-950 bg-red-400">
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Formals</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Casuals</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Party Wears</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Gadgets</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Cosmetics</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Ornaments</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Sanitary Products</Link>
                    </div>
                </div>
                <div className="dropdown inline-block">
                    <button className="text-xl text-red-300 px-5 py-5 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-300">Kids Collections</button>
                    <div className="content drop-shadow-2xl hidden absolute min-w-28 text-red-950 bg-red-400">
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Kids Dresses</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Toys</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Energy Drinks</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Chocolates</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Sports Kits</Link>
                    </div>
                </div>
                <div className="dropdown inline-block">
                    <button className="text-xl text-red-300 px-5 py-5 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-300">Infants</button>
                    <div className="content drop-shadow-2xl hidden absolute min-w-28 text-red-950 bg-red-400">
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Babies dresses</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Diapers</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Toys</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Baby Foods</Link>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}
export default Dropdown