import React from "react";
import { Link } from "react-router-dom";
function DropdownButton(props){
    return(
        <div className="md:hidden">
        <div className="menubutton inline-block">
            <button className="justify-center">
                <span className="material-symbols-outlined text-5xl text-white hover:text-red-300 ease-in-out duration-300">
                    menu
                </span>
            </button>
            <div className="dropdownmenu absolute right-6 px-10 py-3 min-w-24 hidden">
                <div className="dropdownmenu1">
                <Link className="block text-xl bottom-0 text-red-300 w-300 px-5 py-5 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-300" to="#">Men's Collection</Link>
                <div className="dropdowncontent1 right-36 drop-shadow-2xl hidden absolute text-red-950 bg-red-400 w-52">
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Formals</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Casuals</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Party Wears</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Gadgets</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Jackets</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Sports Wears</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Night Dresses</Link>
                </div>
                </div>
                <div className="dropdownmenu2">
                <Link className="block text-xl bottom-0 text-red-300 px-5 py-5 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-300" to="#">Ladies Collections</Link>
                <div className="dropdowncontent2 right-36 drop-shadow-2xl hidden absolute text-red-950 bg-red-400 w-52">
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Formals</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Casuals</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Party Wears</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Gadgets</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Cosmetics</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Ornaments</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Sanitary Products</Link>
                </div>
                </div>
                <div className="dropdownmenu3">
                <Link className="block text-xl bottom-0 text-red-300 px-5 py-5 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-300" to="#">Kids Collections</Link>
                <div className="dropdowncontent3 right-36 drop-shadow-2xl hidden absolute text-red-950 bg-red-400 w-52">
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Kids Dresses</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Toys</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Energy Drinks</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Chocolates</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Sports Kits</Link>
                </div>
                </div>
                <div className="dropdownmenu4">
                <Link className="block text-xl bottom-0 text-red-300 px-5 py-5 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-300" to="#">Infants</Link>
                <div className="dropdowncontent4 right-36 drop-shadow-2xl hidden absolute text-red-950 bg-red-400 w-52">
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Babies dresses</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Diapers</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Toys</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Baby Foods</Link>
                </div>
                </div>
                <div className="dropdownmenu5 border-t-2">
                <Link className="block text-xl bottom-0 text-red-300 px-5 py-5 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-300" to="#">More</Link>
                <div className="dropdowncontent5 right-36 drop-shadow-2xl hidden absolute text-red-950 bg-red-400 w-52">
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="/">Home</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">Products</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">My Items</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">About</Link>
                    <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="#">ContactUs</Link>
                </div>
                </div>
            </div>
        </div>
        </div>
    )
}
export default DropdownButton