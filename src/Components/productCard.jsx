import React from "react";
import { Link } from "react-router-dom";

function ProductCard(props){
    const product = props.product
    return(
        <>
        <article className="m-5">
                    <Link to ={`/products/${product.id}`}>
                        <img className="rounded-xl object-cover w-full" src={product.image} alt="" />
                        <div className="flex flex-col px-2 mt-3">
                            <h3 className="text-lg font-bold">{product.title}</h3>
                            <span>MRP &#8377; {product.price}</span>
                            <span className="text-sm">{`Rating:${product.rating.rate}`}</span>
                        </div>
                    </Link>
                </article>
        </>
    );
}

export default ProductCard;