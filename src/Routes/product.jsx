import React from "react";
import { Link, useLoaderData } from "react-router-dom";
import axios from "axios";

  export async function loader({params}) {
    const response = await axios.get(`https://fakestoreapi.com/products/${params.productId}`);
    const product = response.data  //This line is coz axios give data inside data field
    console.log(product.rating)
    return { product };
  }
function Product(props){
    const { product } = useLoaderData();
    return(
        <>
        <main>
            <section className="container flex flex-col md:grid grid-cols-2 my-16 px-10 gap-20 justify-center items-center">
                <img className="flex items-center w-full px-10" src={product.image} alt="" />
                <div className="flex flex-col justify-center">
                    <h1 className="text-4xl lg:text-6xl font-bold my-10">{product.title}</h1>
                    <span className="my-5 text-2xl">MRP &#8377; {product.price}</span>
                    <div className="flex flex-col bg-red-300 text-red-950 px-10 py-5 rounded-xl w-80 hover:bg-red-950 hover:text-red-300">
                        <span className="text-xl">{`Rating: ${product.rating.rate}`}</span>
                        <span className="text-xl">{`Rating Counts: ${product.rating.count}`}</span>
                        <span className="text-xl">{`Category: ${product.category}`}</span>
                    </div>
                    <p className="my-5 text-2xl">{product.description}</p>
                    <div className="item-center justify-center m-auto my-10 flex flex-col gap-5">
                        <Link className="text-xl text-red-300 bg-red-950 hover:bg-red-300 hover:text-red-950 px-10 py-3 rounded-lg" to={`/products`}>Add To Cart</Link>
                    </div> 
                </div>
            </section>
        </main>
        </>
    );
}

export default Product;