import React from "react";
import axios from "axios";
import { Link, useLoaderData } from "react-router-dom";
import ProductCard from "../Components/productCard";

export async function loader() {
    const response = await axios.get('https://fakestoreapi.com/products');
    const products = response.data  //This line is coz axios give data inside data field
    return { products };
  }

function Products(props){
    const { products } = useLoaderData();
    return(
        <>
            <main className="my-10">
        
                <section className="p-12">
                    <div className="container m-auto">
                        <h2 className="text-5xl p-5 font-bold font-sanserif">Most Popular</h2>
                        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
                            {
                                products.map(product => {
                                return(
                                    <ProductCard key={products.id} product={product} />
                                    )
                                })  
                            }                        
                        </div>
                    </div>
                </section>
            </main>
        </>
    );
}

export default Products;