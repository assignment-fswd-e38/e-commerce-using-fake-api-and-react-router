import React from "react";
import { Outlet, Link } from "react-router-dom";
import Logo from "./images/logo.png"
import DropdownButton from "../Components/dropdownbutton";
import Dropdown from "../Components/dropdown";

export default function Root() {
    return (
      <>
        <header>
        <div className="headertop bg-zinc-900 text-slate-300">
                <div className="container m-auto px-5 flex flex-row items-center justify-between">
                    <div className="flex flex-row">
                        <Link to="#">
                            <img className="w-16 sm:w-24 lg:w-32 xl:w-40" src={Logo} alt="Logo" />
                        </Link>
                        <div className="flex flex-col justify-center ml-10">
                            <span className="text-4xl sm:text-5xl md:text-3xl lg:text-4xl xl:text-5xl font-serif">PRABHATH</span>
                            <span className="font-sans italic hidden sm:flex text-2xl md:text-lg lg:text-xl xl:text-2xl">The World in your fingertip!!</span>
                        </div>
                    </div>
                    <DropdownButton />
                    <nav className="hidden md:flex">
                        <ul className="flex flex-row gap-3 text-base lg:text-lg xl:text-xl">
                            <li>
                                <Link className="hover:font-black hover:text-white cursor-pointer" to="/">Home</Link>
                            </li>
                            <li>
                                <Link className="hover:font-black hover:text-white cursor-pointer" to="/products">Products</Link>
                            </li>
                            <li>
                                <Link className="hover:font-black hover:text-white cursor-pointer" to="#">MyItems</Link>
                            </li>
                            <li>
                                <Link className="hover:font-black hover:text-white cursor-pointer" to="#">About</Link>
                            </li>
                            <li>
                                <Link className="hover:font-black hover:text-white" to="#">ContactUs</Link>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <Dropdown />
        </header>
        <Outlet />
        <footer>

        </footer>
      </>
    );
  }