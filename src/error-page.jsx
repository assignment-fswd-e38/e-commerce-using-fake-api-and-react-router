import { useRouteError } from "react-router-dom";

export default function ErrorPage() {
  const error = useRouteError();
  console.error(error);

  return (
    <div id="error-page w-full h-full">
      <h1 className="text-9xl m-24 flex justify-center content-center">Oops!</h1>
      <p className="text-5xl m-5 flex justify-center content-center">Sorry, an unexpected error has occurred.</p>
      <p className="text-5xl text-red-600 m-5 flex justify-center content-center">
        <i>{error.statusText || error.message}</i>
      </p>
    </div>
  );
}