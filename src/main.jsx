import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom"
import './index.css'
import Root from './Routes/root';
import Home,{ loader as homeLoader} from './Routes/home';
import ErrorPage from './error-page';
import Products,{ loader as productsLoader} from './Routes/products';
import Product, { loader as productLoader} from './Routes/product';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <Home />,
        loader: homeLoader
      },
      {
        path: "/products",
        element: <Products />,
        loader: productsLoader
      },
      {
        path: "/products/:productId",
        element: <Product />,
        loader: productLoader
      }
    ]
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
